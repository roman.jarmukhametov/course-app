"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";

import Link from "next/link";

import { Button } from "@/components/ui/button";

import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";

import { Input } from "@/components/ui/input";

import { loginUser } from "@/app/lib/strapiAuth";

import { useAuth } from "@/context/authContext";

const formSchema = z.object({
  identifier: z.string().min(2, {
    message: "Username must be at least 2 characters.",
  }),

  password: z.string().min(6, {
    message: "Password must be at least 6 characters.",
  }),
});

const SignIn = ({
  setLoading,
}: {
  setLoading: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  // Router instance
  const router = useRouter();
  // State to manage form submission
  const [submitting, setSubmitting] = useState(false);

  const { login } = useAuth();

  // Define a form
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      identifier: "",
      password: "",
    },
  });

  // Handle form submission
  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    setSubmitting(true); // Start submitting
    try {
      const userData = await loginUser(values);
      login(userData); // Use the login function from the auth context
      setLoading(true); // Set loading true when starting redirection
      form.reset(); // Clear form fields
      console.log("User logged in:", userData);
      setTimeout(() => {
        router.push("/"); // Redirect to home page
      }, 2000); // Wait a bit to show the success message before redirecting
    } catch (error) {
      console.error("Login error:", error);
    } finally {
      setSubmitting(false); // End submitting
    }
  };

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        className="space-y-6 px-4 md:px-8 lg:px-10 py-12">
        {/* Username */}
        <FormField
          control={form.control}
          name="identifier"
          render={({ field }) => (
            <FormItem>
              <FormLabel className="text-gray-700 text-base font-bold leading-tight">
                Username
              </FormLabel>
              <FormControl>
                <Input
                  placeholder="Please enter your username"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        {/* Password */}
        <FormField
          control={form.control}
          name="password"
          render={({ field }) => (
            <FormItem>
              <FormLabel className="text-gray-700 text-base font-bold leading-tight">
                Password
              </FormLabel>
              <FormControl>
                <Input
                  type="password"
                  placeholder="Please enter your password"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <Button
          className="w-full"
          type="submit"
          disabled={submitting} // Disable button while submitting
        >
          {submitting ? "Logging in..." : "Login"}
        </Button>

        <p className="text-gray-700 text-base font-normal">
          If you have no an account you may
          <Link
            href="/signup"
            className="ml-1 font-bold hover:text-gray-900 hover:underline transition-all ease-in-out">
            Sign Up
          </Link>
        </p>
      </form>
    </Form>
  );
};

export default SignIn;
