/**
 * A React component to display a list of courses fetched from a Strapi backend.
 *
 * The `Courses` component is responsible for fetching course data asynchronously
 * from a specified endpoint using the Strapi API. It renders different UI states based
 * on the data fetched: a loading spinner, an empty course list, or a list of course cards.
 * Each course card displays detailed information about the course, such as title,
 * description, authors, duration, and creation date.
 *
 * The component uses `useState` to manage the state of the courses and loading status.
 * `useEffect` is utilized to perform the data fetching on component mount.
 *
 * Components used:
 * - `Loader`: A visual indicator for loading state.
 * - `EmptyCourseList`: Displayed when no courses are available after fetching.
 * - `SearchInput`: Allows filtering or searching through the list of courses.
 * - `PrimaryButton`: A button for adding a new course.
 * - `CourseCard`: Displays individual course details.
 *
 * @component
 * @example
 * // This is how you can render the Courses component in a React application
 * <Courses />
 *
 * Dependencies:
 * - getStrapiData: A utility function to fetch data from Strapi.
 * - Course: A TypeScript type representing the structure of course data.
 */

"use client";

import { useEffect, useState } from "react";
import CourseCard from "@/components/ui/CourseCard";
import SearchInput from "@/components/ui/SearchInput";
import PrimaryButton from "@/components/ui/PrimaryButton";
import EmptyCourseList from "@/components/ui/EmptyCourseList";
import Loader from "@/components/ui/Loader";

import getStrapiData from "@/app/lib/getStrapiData";

import { Course } from "@/app/lib/types";

import { useAuth } from "@/context/authContext";

const Courses = () => {
  // State for storing the list of courses and the loading status.
  const [courses, setCourses] = useState<Course[]>([]);
  const [loading, setLoading] = useState(true);

  const { user } = useAuth();

  const showButtons = !!user;

  // Fetch courses data from the API on component mount.
  useEffect(() => {
    const fetchData = async () => {
      try {
        const coursesData = await getStrapiData("courses?populate=deep");

        // Set the fetched courses to state and update loading status.
        setCourses(coursesData.data);
        setLoading(false);
      } catch (error) {
        console.error("Failed to fetch courses: ", error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  // Conditional rendering based on the loading and courses state.
  if (loading) {
    return <Loader />;
  }

  if (!loading && courses.length === 0) {
    return <EmptyCourseList />;
  }

  return (
    <section className="py-20 mb-10 px-4">
      <div className="flex flex-col lg:flex-row gap-4 justify-between mb-10">
        <SearchInput />
        {showButtons && <PrimaryButton textOnly="Add New Course" />}
      </div>

      {courses.map((course) => {
        const authors = course.attributes.authors.data
          .map((author) => author.attributes.name)
          .join(", ");
        const courseSlug = `/courses/${course.attributes.slug}`; // Construct the slug path

        return (
          <CourseCard
            key={course.id}
            title={course.attributes.title}
            description={course.attributes.description}
            authors={authors}
            duration={course.attributes.duration}
            creationDate={course.attributes.creationDate}
            slug={courseSlug} // Pass slug as a prop
          />
        );
      })}
    </section>
  );
};

export default Courses;
