/**
 * Displays a single course card with details such as title, description, authors, duration, and creation date.
 * This component is used to present key information about a course in a visually appealing card format.
 * It includes buttons for interacting with the course, like viewing, editing, and deleting.
 *
 * Props:
 * @param {string} title - The title of the course. Defaults to "Default Course Title".
 * @param {string} description - A brief description of the course. Defaults to a placeholder text.
 * @param {string} authors - Comma-separated names of the authors of the course. Defaults to "John Doe, Jane Doe".
 * @param {number} duration - The duration of the course in minutes. Defaults to 230.
 * @param {string} creationDate - The date the course was created. Defaults to "10/11/2020".
 *
 * @component
 * @example
 * <CourseCard
 *   title="Introduction to React"
 *   description="Learn the basics of React, including JSX, components, and state management."
 *   authors="Jane Doe, John Appleseed"
 *   duration={180}
 *   creationDate="01/01/2021"
 * />
 *
 * Returns:
 * A styled card with course information and action buttons.
 */

import { useState, useEffect } from "react";

import PrimaryButton from "@/components/ui/PrimaryButton";
import formatDuration from "@/app/lib/formatDuration";
import formatDate from "@/app/lib/formatDate";

import { useAuth } from "@/context/authContext";

interface CourseCardProps {
  title?: string;
  description?: string;
  authors?: string;
  duration?: number;
  creationDate?: string;
  slug: string;
}

const CourseCard: React.FC<CourseCardProps> = ({
  title = "Default Course Title",
  description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.. Lorem Ipsum is simply dummy text of the printing and typesetting industryLorem Ipsum is simply dummy text of the printing and typesetting industryLorem Ipsum is simply dummy text of the printing and typesetting industry",
  authors = "John Doe, Jane Doe",
  duration = 230,
  slug,
  creationDate = "10/11/2020",
}) => {
  const { user } = useAuth();

  const showButtons = !!user;

  return (
    <div className="flex flex-col md:flex-row gap-4 md:gap-10 px-8 py-10 bg-white shadow rounded border-l-4 border-gray-900 mb-8">
      {/* Left part */}
      <div className="w-full md:w-2/3">
        <h3 className="text-gray-900 text-lg md:text-xl font-bold uppercase leading-loose mb-3">
          {title}
        </h3>
        <p className="text-gray-700 text-sm md:text-base font-normal leading-tight">
          {description}
        </p>
      </div>
      {/* Right part */}
      <div className="w-full md:w-1/3 flex flex-col gap-2">
        {/* Course info */}
        <div className="flex flex-col gap-2">
          <p className="text-gray-700 text-sm md:text-base font-normal leading-tight">
            <strong>Authors:</strong> {authors}
          </p>
          <p className="text-gray-700 text-sm md:text-base font-normal leading-tight">
            <strong>Duration:</strong> {formatDuration(duration)}
          </p>
          <p className="text-gray-700 text-sm md:text-base font-normal leading-tight">
            <strong>Created:</strong> {formatDate(creationDate)}
          </p>
        </div>

        {/* Buttons  */}
        <div className="flex flex-col sm:flex-row gap-2 items-center">
          <PrimaryButton
            textOnly="Show Course"
            aria-label="Show course"
            href={slug}
          />

          {/* Only show for a logged in user */}

          {showButtons && (
            <>
              <PrimaryButton
                iconTrash
                aria-label="Delete course"
              />

              <PrimaryButton
                iconEdit
                aria-label="Edit course"
              />
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default CourseCard;
