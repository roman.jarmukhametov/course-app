/**
 * Displays a message indicating that no courses are available and provides an action button to add a new course.
 * This component is typically used in a courses management application where it can visually indicate that the
 * user's course list is currently empty and prompt them to add a new course.
 *
 * The component renders a centered message within a full-screen container and includes a button that when
 * clicked would typically navigate the user to a form for adding a new course.
 *
 * @component
 * @returns {React.ReactNode} A React component that displays an empty state message with an action button.
 *
 * @example
 * // This is how you can render the EmptyCourseList component within a React application:
 * <EmptyCourseList />
 */

import PrimaryButton from "@/components/ui/PrimaryButton";

const EmptyCourseList = () => {
  return (
    <section className="container mx-auto h-screen flex flex-col items-center justify-center">
      <h2 className="text-2xl text-gray-700 font-bold mb-2">
        Your List Is Empty
      </h2>
      <p className="mb-10 text-base text-gray-700 font-normal leading-10">
        Please use ’Add New Course’ button to add your first course
      </p>
      <PrimaryButton textOnly="Add New Course" />
    </section>
  );
};

export default EmptyCourseList;
