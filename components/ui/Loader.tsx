import styles from "./loader.module.css";

const Loader = () => {
  return (
    <div className={`${styles.container} h-screen`}>
      <div className={styles.loader}></div>
    </div>
  );
};

export default Loader;
