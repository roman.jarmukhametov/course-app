/**
 * A React component that displays detailed information about a specific course.
 * The `CourseInfo` component fetches course data based on the course's slug extracted from the current URL path.
 * It shows various course attributes such as title, description, duration, creation date, and authors.
 * If the course data is still loading, a loader animation is displayed. If no course data is found, a message
 * is shown indicating that no course is available.
 *
 * The component makes use of the Next.js `usePathname` hook to derive the current URL path, and fetches
 * course data using a custom utility function `getStrapiData`.
 *
 * States:
 * - `course`: Holds the course data or null if no data is found.
 * - `loading`: A boolean indicating whether the course data is being fetched.
 *
 * @component
 * @returns {JSX.Element} A section containing detailed course information or messages about the course loading status.
 *
 * @example
 * // This is how you can render the CourseInfo component within a React application to display course details:
 * <CourseInfo />
 *
 * Dependencies:
 * - `PrimaryButton`: A component for rendering a button.
 * - `Loader`: A component for showing a loading indicator.
 * - `formatDuration`: A function to format the duration of the course.
 * - `formatDate`: A function to format the creation date of the course.
 */

"use client";

import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import PrimaryButton from "./PrimaryButton";
import Loader from "@/components/ui/Loader";

import getStrapiData from "@/app/lib/getStrapiData";

import { Course } from "@/app/lib/types";

import formatDuration from "@/app/lib/formatDuration";
import formatDate from "@/app/lib/formatDate";

const CourseInfo = () => {
  const pathname = usePathname();
  const slug = pathname.split("/").pop(); // Extract the slug from the pathname

  // State for storing the current course and the loading status.
  const [course, setCourse] = useState<Course | null>(null);
  const [loading, setLoading] = useState(true);

  // Fetch course data from the API on component mount.
  useEffect(() => {
    const fetchData = async () => {
      try {
        // Add the slug to the query
        const response = await getStrapiData(
          `courses?populate=deep&filters[slug][$eq]=${slug}`
        );
        const fetchedCourse =
          response.data.length > 0 ? response.data[0] : null;

        // Set the fetched course to state and update loading status.
        setCourse(fetchedCourse);
        setLoading(false);
      } catch (error) {
        console.error("Failed to fetch course: ", error);
        setLoading(false);
      }
    };

    fetchData();
  }, [slug]); // Re-fetch if slug changes

  if (loading) {
    return <Loader />;
  }

  if (!course) {
    return (
      <div className="flex flex-col items-center justify-center h-screen">
        <p className="text-4xl text-gray-700 font-bold tracking-wider">
          No course found.
        </p>
      </div>
    ); // Display if no course is found
  }

  // Extract and join the author names
  const authorNames = course.attributes.authors.data
    .map((author) => author.attributes.name)
    .join(", ");

  // Return course details
  return (
    <section className="py-20 mb-10 px-4 flex flex-col gap-9">
      <h1 className="text-2xl font-bold text-gray-700 capitalize leading-10">
        {course.attributes.title}
      </h1>

      <div className="flex flex-row px-8 py-10 bg-white rounded border border-stone-300 divide-x divide-stone-300">
        {/* Left Part */}
        <div className="flex-1 p-4">
          <p className="text-gray-700 text-xl font-bold leading-tight mb-5">
            Description:
          </p>
          <p> {course.attributes.description}</p>
        </div>

        {/* Right Part */}
        <div className="flex-1 p-4">
          <p className="mb-3 text-gray-700 text-base font-normal leading-tight">
            <strong className="text-xl leading-loose">ID:</strong>{" "}
            {course.attributes.courseId}
          </p>

          <p className="mb-3 text-gray-700 text-base font-normal leading-tight">
            <strong className="text-xl leading-loose">Duration:</strong>{" "}
            {formatDuration(course.attributes.duration)}
          </p>

          <p className="mb-3 text-gray-700 text-base font-normal leading-tight">
            <strong className="text-xl  leading-loose">Created:</strong>{" "}
            {formatDate(course.attributes.creationDate)}
          </p>

          <p className="mb-3 text-gray-700 text-base font-normal leading-tight">
            <strong className="text-xl  leading-loose">Authors:</strong>{" "}
            {authorNames}
          </p>
        </div>
      </div>

      <div className="flex justify-end">
        <PrimaryButton
          textOnly="Back"
          href="/"
        />
      </div>
    </section>
  );
};

export default CourseInfo;
