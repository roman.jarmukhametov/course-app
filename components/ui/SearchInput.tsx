"use client";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";

import { Button } from "@/components/ui/button";

import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
} from "@/components/ui/form";

import { Input } from "@/components/ui/input";

const formSchema = z.object({
  searchRequest: z.string(),
});

const SearchInput = () => {
  // Define a form
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      searchRequest: "",
    },
  });

  // Handle form submission
  const onSubmit = async (data: z.infer<typeof formSchema>) => {
    console.log(data);
  };

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        className="flex flex-col lg:flex-row gap-2 items-center">
        {/* Username */}
        <FormField
          control={form.control}
          name="searchRequest"
          render={({ field }) => (
            <FormItem className="">
              <FormControl>
                <Input
                  className="block w-full lg:w-[400px]"
                  placeholder="Type something to search"
                  {...field}
                />
              </FormControl>
            </FormItem>
          )}
        />

        <Button
          className=" w-full inline-flex items-center justify-center"
          type="submit">
          Search
        </Button>
      </form>
    </Form>
  );
};

export default SearchInput;
