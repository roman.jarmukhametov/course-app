/**
 * A React component for user sign-up. It provides a form for users to input their username, email,
 * and password and submit these details to register. The form uses Zod for validation and
 * `react-hook-form` for form state management and submission handling.
 *
 * The component integrates error handling and displays loading states to provide feedback during
 * the registration process. On successful registration, the user is redirected to the homepage.
 *
 * Props:
 * @param {React.Dispatch<React.SetStateAction<boolean>>} setLoading - A function from parent component
 * to control loading state globally, typically used to display a loading spinner or similar indicators
 * while navigating to another page after successful form submission.
 *
 * Components used:
 * - `Button`: A UI component for rendering the submit button.
 * - `Input`: A UI component for rendering input fields.
 * - `Form`, `FormField`, `FormItem`, `FormLabel`, `FormControl`, `FormMessage`: Components used to
 * construct the form with appropriate validation and error messaging.
 *
 * @returns {JSX.Element} A form that allows users to sign up by providing their username, email, and password.
 *
 * @example
 * // How to render the SignUp component
 * <SignUp setLoading={setGlobalLoading} />
 */

"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";

import Link from "next/link";

import { Button } from "@/components/ui/button";

import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";

import { Input } from "@/components/ui/input";

import { registerUser } from "@/app/lib/strapiAuth";

import { useAuth } from "@/context/authContext";

const formSchema = z.object({
  username: z.string().min(2, {
    message: "Username must be at least 2 characters.",
  }),
  email: z.string().email({
    message: "Invalid email address.",
  }),
  password: z.string().min(6, {
    message: "Password must be at least 6 characters.",
  }),
});

const SignUp = ({
  setLoading,
}: {
  setLoading: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  // Router instance
  const router = useRouter();
  // State to manage form submission
  const [submitting, setSubmitting] = useState(false);

  const { login } = useAuth();

  // Define a form
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      username: "",
      email: "",
      password: "",
    },
  });

  // Handle form submission
  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    setSubmitting(true); // Start submitting
    try {
      const userData = await registerUser(values);
      login(userData); // Use the login function from the auth context
      setLoading(true); // Set loading true when starting redirection
      form.reset(); // Clear form fields
      console.log("User registered:", userData);
      setTimeout(() => {
        router.push("/"); // Redirect to home page
      }, 2000); // Wait a bit to show the success message before redirecting
    } catch (error) {
      console.error("Registration error:", error);
    } finally {
      setSubmitting(false); // End submitting
    }
  };

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        className="space-y-6 px-4 md:px-8 lg:px-10 py-12">
        {/* Username */}
        <FormField
          control={form.control}
          name="username"
          render={({ field }) => (
            <FormItem>
              <FormLabel className="text-gray-700 text-base font-bold leading-tight">
                Username
              </FormLabel>
              <FormControl>
                <Input
                  placeholder="Please enter your username"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        {/* Email */}
        <FormField
          control={form.control}
          name="email"
          render={({ field }) => (
            <FormItem>
              <FormLabel className="text-gray-700 text-base font-bold leading-tight">
                Email
              </FormLabel>
              <FormControl>
                <Input
                  type="email"
                  placeholder="Please enter your email"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        {/* Password */}
        <FormField
          control={form.control}
          name="password"
          render={({ field }) => (
            <FormItem>
              <FormLabel className="text-gray-700 text-base font-bold leading-tight">
                Password
              </FormLabel>
              <FormControl>
                <Input
                  type="password"
                  placeholder="Please enter your password"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <Button
          className="w-full"
          type="submit"
          disabled={submitting} // Disable button while submitting
        >
          {submitting ? "Creating account..." : "Create account"}
        </Button>

        <p className="text-gray-700 text-base font-normal">
          If you have an account you may
          <Link
            href="/signin"
            className="ml-1 font-bold hover:text-gray-900 hover:underline transition-all ease-in-out">
            Login
          </Link>
        </p>
      </form>
    </Form>
  );
};

export default SignUp;
