"use client";

import { motion } from "framer-motion";

export default function Template({ children }: { children: React.ReactNode }) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }} // Define the exit animation for when the component unmounts
      transition={{ ease: "easeInOut", duration: 0.75 }}>
      {children}
    </motion.div>
  );
}
