export const mockedCoursesList = [
  {
    id: "de5aaa59-90f5-4dbc-b8a9-aaf205c551ba",
    title: "JavaScript",
    description: `Unlock the Power of JavaScript: The Language of the Web. Dive deep into JavaScript, the heartbeat of the modern web, 
                  and master the key concepts that drive the interactive and dynamic aspects of powerful web applications.`,
    creationDate: "08/03/2021",
    duration: 160,
    authors: [
      "27cc3006-e93a-4748-8ca8-73d06aa93b6d",
      "f762978b-61eb-4096-812b-ebde22838167",
    ],
  },
  {
    id: "b5630fdd-7bf7-4d39-b75a-2b5906fd0916",
    title: "Angular",
    description: `Embark on a journey to master Angular, one of the most efficient and popular frameworks for building web applications. 
                    This comprehensive course covers everything from the basics of TypeScript, components, and directives 
                    to more advanced concepts like services, routing, and state management with RxJS and NgRx. `,
    creationDate: "10/11/2020",
    duration: 210,
    authors: [
      "df32994e-b23d-497c-9e4d-84e4dc02882f",
      "095a1817-d45b-4ed7-9cf7-b2417bcbf748",
    ],
  },

  {
    id: "cd5f6483-d4f7-4567-d8f9-ea204c553456",
    title: "React for Beginners",
    description: `Dive into the world of React! Learn the basics of React, including components, state management, and the React lifecycle. 
                    Build dynamic and interactive web applications with ease.`,
    creationDate: "02/15/2022",
    duration: 120,
    authors: [
      "67cc3006-a93a-4848-9ca8-73d06aa93b6e",
      "f762978b-61eb-4096-812b-ebde22838167", // Nicolas Kim
    ],
  },
  {
    id: "a8349fdd-48b7-4d11-b75a-2b5906fd0543",
    title: "Advanced CSS Techniques",
    description: `Explore advanced CSS techniques to take your web designs to the next level. Learn about flexbox, grid, animations, transitions, 
                    and more to create visually compelling websites.`,
    creationDate: "07/22/2021",
    duration: 180,
    authors: [
      "27cc3006-e93a-4748-8ca8-73d06aa93b6d", // Vasiliy Dobkin
      "a762978b-61eb-4096-812b-ebde228382f8",
    ],
  },
  {
    id: "e65f7483-c4f7-4567-e8f9-ea204c550123",
    title: "Vue.js Comprehensive Guide",
    description: `From zero to hero with Vue.js! This course covers everything you need to know to start building applications with Vue.js, 
                    including the Vue CLI, Vuex, Vue Router, and more.`,
    creationDate: "09/11/2021",
    duration: 150,
    authors: [
      "a762978b-61eb-4096-812b-ebde228382f8",
      "095a1817-d45b-4ed7-9cf7-b2417bcbf748", // Valentina Larina
    ],
  },
];

export const mockedAuthorsList = [
  {
    id: "27cc3006-e93a-4748-8ca8-73d06aa93b6d",
    name: "Vasiliy Dobkin",
  },
  {
    id: "f762978b-61eb-4096-812b-ebde22838167",
    name: "Nicolas Kim",
  },
  {
    id: "df32994e-b23d-497c-9e4d-84e4dc02882f",
    name: "Anna Sidorenko",
  },
  {
    id: "095a1817-d45b-4ed7-9cf7-b2417bcbf748",
    name: "Valentina Larina",
  },

  {
    id: "67cc3006-a93a-4848-9ca8-73d06aa93b6e",
    name: "Emily Dawson",
  },
  {
    id: "a762978b-61eb-4096-812b-ebde228382f8",
    name: "Luke White",
  },
];
