// @/lib/strapiAuth.ts

import axios from "axios";

interface RegistrationData {
  username: string;
  email: string;
  password: string;
}

interface LoginData {
  identifier: string; // Username or Email
  password: string;
}

export const registerUser = async (registrationData: RegistrationData) => {
  try {
    const response = await axios.post(
      "http://localhost:1337/api/auth/local/register",
      registrationData
    );
    return response.data;
  } catch (error) {
    if (axios.isAxiosError(error) && error.response) {
      // Handle the error response from Strapi here

      throw new Error(error.response.data.error.message);
    } else {
      // Handle unknown errors
      throw new Error("An unknown error occurred during registration.");
    }
  }
};

export const loginUser = async (loginData: LoginData) => {
  try {
    const response = await axios.post(
      "http://localhost:1337/api/auth/local",
      loginData
    );
    return response.data;
  } catch (error) {
    if (axios.isAxiosError(error) && error.response) {
      // Handle the error response from Strapi here, often error.response.data.message
      throw new Error(error.response.data.error.message);
    } else {
      // Handle unknown errors
      throw new Error("An unknown error occurred during login.");
    }
  }
};
