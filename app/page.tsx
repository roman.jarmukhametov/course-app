import Courses from "@/components/ui/Courses";

export default function Home() {
  return (
    <main className="max-w-7xl mx-auto">
      <Courses />
    </main>
  );
}
