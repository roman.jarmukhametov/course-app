import { AuthProvider } from "@/context/authContext";
import type { Metadata } from "next";
import { openSans } from "@/components/ui/fonts";
import "./globals.css";
import Navbar from "@/components/ui/Navbar";

export const metadata: Metadata = {
  title: "Course App",
  description: "EPAM Course App",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <AuthProvider>
        <body
          className={`${openSans.className} antialiased h-screen w-screen bg-neutral-100`}>
          <Navbar />
          {children}
        </body>
      </AuthProvider>
    </html>
  );
}
