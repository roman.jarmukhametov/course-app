import CourseInfo from "@/components/ui/CourseInfo";

const DetailedCoursePage = () => {
  return (
    <main className="max-w-7xl mx-auto">
      <CourseInfo />
    </main>
  );
};

export default DetailedCoursePage;
