"use client";

import { useState } from "react";
import SignUp from "@/components/ui/SignUp";
import Loader from "@/components/ui/Loader";

const Signup = () => {
  const [loading, setLoading] = useState(false);

  return (
    <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
      {loading ? <Loader /> : null}

      <div className="sm:mx-auto sm:w-full sm:max-w-sm">
        <h1 className="text-gray-700 font-bold capitalize leading-tight sm:leading-10 sm:text-2xl md:text-4xl lg:text-4xl text-center ">
          Create Account
        </h1>
      </div>

      <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-2xl bg-white border-[1px] rounded">
        <SignUp setLoading={setLoading} />
      </div>
    </div>
  );
};

export default Signup;
